document.addEventListener("DOMContentLoaded", async () => {
  const verseDiv = document.querySelector("#verse");
  const referenceDiv = document.querySelector("#reference");
  const versionDiv = document.querySelector("#version");
  const verseBtn = document.querySelector("#fetch-verse");
  const closePopup = document.querySelector("#close-popup");
  const copyBtn = document.querySelector("#copy-verse");
  const contentContainer = document.querySelector("#content-container");
  const loadingDiv = document.querySelector("#loading");

  closePopup.addEventListener("click", function () {
    window.close();
  });

  fetch("https://beta.ourmanna.com/api/v1/get/?format=json&order=random")
    .then((res) => res.json())
    .then(({ verse }) => {
      loadingDiv.classList.add("hidden");
      verseDiv.innerHTML = verse.details.text;
      referenceDiv.innerHTML = verse.details.reference;
      versionDiv.innerHTML = `(${verse.details.version})`;
      contentContainer.classList.remove("hidden");
    })
    .catch((error) => alert(error));

  verseBtn.addEventListener("click", function () {
    fetch("https://beta.ourmanna.com/api/v1/get/?format=json&order=random")
      .then((res) => res.json())
      .then(({ verse }) => {
        verseDiv.innerHTML = verse.details.text;
        referenceDiv.innerHTML = verse.details.reference;
        versionDiv.innerHTML = `(${verse.details.version})`;
      })
      .catch((error) => alert(error));
  });

  // copy text
  copyBtn.addEventListener("click", function () {
    const el = document.createElement("textarea");
    el.value = `${verseDiv.innerHTML} - ${referenceDiv.innerHTML} ${versionDiv.innerHTML}`;
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
  });
});
